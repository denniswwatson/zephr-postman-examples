# Zephr Postman Examples
This repo includes various example requests to Zephr endpoints to help you get familiar with the Zephr API.

## Postman environment template
You can find a template environment file for Postman in the `utils` directory. This will allow you to set the following basic variables for your calls to the Zephr APIs:
- `zephrAccessKey`
- `zephrSecretKey`
- `zephrDomain` (this will either be your V3 admin console domain, or `console.zephr.com` if you're using V4)

## Authentication
For details on authenticating with with Zephr endpoints, please see the pre-request script in the `utils` directory (this script is also pre-packaged with each request in Postman collections in the repository).

## Disclaimer
These Postman requests are provided as examples: they may not fit your use case. Whilst every effort will be made to keep these requests up-to-date with the latest versions of the Zephr APIs, the Zephr Admin API documentation should be treated as the authoritative resource: https://support.zephr.com/admin-api.