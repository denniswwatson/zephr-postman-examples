# Import users

This collection demonstrates importing users to Zephr from a different identity store. For each user, the data to be imported falls into 3 main categories:

1. Identifier: usually an email address
2. Validator: password hash information. This is provided as a base64 encoded representation of a stringified JSON object. For more information on what to include in the password hash object, please consult [this documentation](https://support.zephr.com/documentation/settings/extensions/login-extension). Please ask the system administrator for your source identity store if you need further information on how existing passwords have been hashed.
3. Attributes: user attributes to be imported for users.

## Pre-requisites

- Postman (collection tested in v7.36.1)
- Zephr Access Key and Secret Key
- Zephr User Attributes set up in your Zephr tenant
- CSV file with:
    - User email addresses
    - User password hash information (this must be provided as a Base64 encoded stringified JSON object - see below for more details)
    - User attribute values
    
### Preparing the password hash information

1. For each user, create a JSON object and add the relevant pieces of information for the password hash from your source identity store (you can check [this documentation](https://support.zephr.com/documentation/settings/extensions/login-extension) to find which fields you will need depending on how user passwords have been hashed). E.g.:
```json
{
    "algorithm": "md5",
    "hash": "cc58db7c46ddbee969c257af0c505498",
    "salt": "mySuperSecureSalt"
}
```
2. Stringify your object. E.g.:
```text
{\n    \"algorithm\": \"md5\",\n    \"hash\": \"cc58db7c46ddbee969c257af0c505498\",\n    \"salt\": \"mySuperSecureSalt\"\n}
```
3. Base64 encode your stringified object. E.g.:
```text
e1wiYWxnb3JpdGhtXCI6IFwibWQ1XCIsXCJoYXNoXCI6IFwiY2M1OGRiN2M0NmRkYmVlOTY5YzI1N2FmMGM1MDU0OThcIiwgXCJzYWx0XCI6IFwibXlTdXBlclNlY3VyZVNhbHRcIn0=
```

### CSV example

Please see below for an example CSV with fields for:

- `email_address`
- `legacy_password_base64`
- `first-name`*
- `last-name`*
- `newsletter-opt-in`*

\* Zephr User Attribute

```text
email_address,legacy_password_base64,first-name,last-name,newsletter-opt-in
test.user@company.com,e1wiYWxnb3JpdGhtXCI6IFwibWQ1XCIsXCJoYXNoXCI6IFwiY2M1OGRiN2M0NmRkYmVlOTY5YzI1N2FmMGM1MDU0OThcIiwgXCJzYWx0XCI6IFwibXlTdXBlclNlY3VyZVNhbHRcIn0=,Test,User,true
sample.person@anothercompany.com,e1wiYWxnb3JpdGhtXCI6IFwibWQ1XCIsXCJoYXNoXCI6IFwiQzBQY1B2Z3B3OFoydTB1RGFmV1Y5UmxaUXp0ZGNDZ3JcIiwgXCJzYWx0XCI6IFwibXlFdmVuTW9yZVN1cGVyU2VjdXJlU2FsdFwifQ==,Sample,Person,false
```

Note: for user attributes set to type `checkbox` (boolean), please use a value of either `true` or `false`.

## Using the collection

1. Make sure you've got your Postman environment set up (you can use the template environment file in the `utils` directory in the root of this repo)
2. Import the 'Import users' collection to Postman
3. Open the collection Runner (Runner button near top left)
4. Click 'Import users' in the list of collections
5. You should see a single API call of 'Import user' selected in the Run Order pane 
6. Select your environment from the 'Environment dropdown'
7. For the 'Data' field, click the 'Select File' button and select your CSV file (you can use the 'Preview' button to check how Postman sees the values in your file)
8. Click the blue 'Run Import users' button
9. Check the responses on the results page: a 201 response indicates an entitlement being successfully granted to a user
