# Grant entitlements to users
This collection demonstrates simple assignment of entitlements to users (without start or end times).

## Pre-requisites:
- Postman (collection tested in v7.31.1)
- Zephr Access Key and Secret Key
- CSV with relevant columns:
    - To add entitlements with no start/end time to customers, `userId`,`entitlementId` are needed
    - To add entitlements with a start/end time to customers, `userId`,`entitlementId`,`startTime`,`endTime` are needed. Note: `startTime` or `endTime` or both may be an empty value for any of the customer rows on this call, but all four columns must be provided for each row

## Configuration
1. Import the `utils/zephr.postman_enivironment.json` file to Postman (environment settings new top right)
2. Give the environment a suitable name as the value of the `name` key
3. Add values for `zephrAccessKey` & `zephrSecretKey`
4. For `zephrDomain`:
    - if you are using the V4 console, use `console.zephr.com`
    - if you are using the V3 console, use the subdomain on which you access the console (e.g. `<your_site>.admin.blaize.io`)
5. Import the collection to Postman (Import button near top left)

## Using the collection
### Fetching your entitlement ID(s)
1. Ensure you have your `zephrAccessKey`, `zephrSecretKey` and `zephrDomain` set in your environment
2. Select the `Get Entitlement IDs` request and click 'Send'
3. You will be returned an array of Zephr product records, you will be able to find the relevant entitlement ID at the `entitlement.id` key of a given product

### Adding entitlements to users
1. Open the collection Runner (Runner button near top left)
2. Click 'Grant entitlements to users' in the list of collections
3. In the 'Run Order' section, deselect all requests, then select the relevant `Assign entitlement to user...` request
4. Select your environment from the 'Environment dropdown'
5. For the 'Data' field, click the 'Select File' button and select your CSV file
6. Click the blue 'Run <collection_name>' button
7. Check the responses on the results page: a 201 response indicates an entitlement being successfully granted to a user

## Example CSV
### No start/end times
```csv
userId,entitlementId
16916726-c767-46bf-8d28-a3582bc7c1d0,401cc929-5e3d-48a2-b366-6f648d6bd942
46638428-e9c4-49e8-889e-4601736100c5,401cc929-5e3d-48a2-b366-6f648d6bd942
09523716-8f5c-4f82-83b4-ffaee9450f07,401cc929-5e3d-48a2-b366-6f648d6bd942
```
### With start/end times
```csv
userId,entitlementId,startTime,endTime
16916726-c767-46bf-8d28-a3582bc7c1d0,401cc929-5e3d-48a2-b366-6f648d6bd942,2020-10-20 15:00:02,2021-10-20 15:00:02
46638428-e9c4-49e8-889e-4601736100c5,401cc929-5e3d-48a2-b366-6f648d6bd942,2020-10-20 15:00:02,2021-10-20 15:00:02
09523716-8f5c-4f82-83b4-ffaee9450f07,401cc929-5e3d-48a2-b366-6f648d6bd942,,
ad3c150a-1e73-4dd3-a1d8-3ef8fbb677ae,401cc929-5e3d-48a2-b366-6f648d6bd942,,2021-10-20 15:00:02
e55a3c50-9886-4ac4-a6a1-1ac4446552e8,401cc929-5e3d-48a2-b366-6f648d6bd942,2020-10-20 15:00:02,
```
Notes:
- In the examples above, all users are being given the same entitlement: this is just for illustrative purposes, you can assign different entitlements to different users as you see fit on each row.
- In the examples with start times and end times, you will see that is is possible to provide no value for either the start time, end time or both on a row-by-row basis. The only requirement is that all four fields are present for each row: but the fields for start/end times may contain no value.

## CSV Data types (all strings)
- `userId`: UUIDv4 (e.g. `a575da06-aed3-43d3-88f2-8b5efcbee141`)
- `entitlementId`: UUIDv4 (e.g. `ef133fcb-48ca-4085-abed-619a222bdc3d`)
- `startTime`: a string representation of a MySQL dateTime. Format should be `YYYY-mm-dd HH:MM:SS` (e.g. `2020-10-03 15:16:00`)
- `endTime`: a string representation of a MySQL dateTime. Format should be `YYYY-mm-dd HH:MM:SS` (e.g. `2021-10-03 15:15:59`)