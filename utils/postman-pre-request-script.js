const accessKey = pm.variables.get('zephrAccessKey')
const secretKey = pm.variables.get('zephrSecretKey')

// expanding variables in URL with pm.variables.replaceIn, then constructing path
const path = `/${pm.variables.replaceIn(pm.request.url.path).join('/')}`
const timestamp = (new Date()).getTime().toString()
const nonce = (Math.random()).toString()

let hash = CryptoJS.algo.SHA256.create()
hash.update(secretKey)

if (pm.request.body && Object.keys(pm.request.body).length) {
    // expanding variables in body before updating hash with payload
    hash.update(pm.variables.replaceIn(pm.request.body.raw))
}

hashString = hash.update(path)
    .update(pm.request.url.getQueryString())
    .update(request.method)
    .update(timestamp)
    .update(nonce)
    .finalize()
    .toString()

pm.environment.set('zephrAuthHeader', `ZEPHR-HMAC-SHA256 ${accessKey}:${timestamp}:${nonce}:${hashString}`);
