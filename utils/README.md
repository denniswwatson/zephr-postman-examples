# Postman pre-request script

This script takes care of HMAC signing requests destined for Zephr endpoints.

To use this script in a request, please ensure you have the following variables set in your Postman environment:
- `zephrAccessKey`
- `zephrSecretKey`
